# Commits

## Organisation

* Les comptes-rendus des réunions sont rangés dans le dossier "Réunion"
* Les programmes créés sont disponibles dans le dossier "Programmes"
* Le compte de Samy Salamor étant associé au RaspberryPi, une convention a été choisie de mentionner Samy&Quentin dans le nom du commit lorsque le travail a été effectué à deux


## Détails

### 12/02/18
* (Samy)  Début du codage d'une interface permettant de contrôler le robot Sphero en python avec Tkinter

### 13/02/18
* (Samy) Implémentation des fonctions permettant de diriger le robot à l'aide des touches du clavier + amélioration de l'interface graphique

### 14/02/18
* (Samy&Quentin) Tests et corrections du programme qui est désormais fonctionnel et qui permet de diriger facilement le robot Sphero + appropriation du fonctionnement de l'API sphero_sprk et implémentation de fonctions inexistantes dans la version téléchargée sur GitHub (cf. Sources) avec notamment l'implémentation de get_power_state (connaître l'état de la batterie)



# Project Goals

## Sources 

* A working python module [https://github.com/CMU-ARM/sphero_sprk]
* Installing and configuring a Raspberry Pi [https://www.raspberrypi.org/help/]
* The official SDK and Documentation [https://github.com/orbotix]
* The official documentation [http://sdk.sphero.com/]


# Roadmap

## Requirements 

* Linux (bluepy & bluez dependence)
* >= python3.3
* CMU-ARM/sphero_sprk (the link above)
* PyGame [http://www.pygame.org/] user interface and joystick handling
* Crazyflie clients [https://github.com/bitcraze/crazyflie-clients-python], starting point to get some ideas for UI design

* Virtualenv (to keep you official python installation clean)


