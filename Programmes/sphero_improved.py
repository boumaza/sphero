from sphero_sprk import *

class Sphero_Improved(Sphero):
    
    def __init__(self, addr=None):
        if(addr == None):
            #search for sphero
            sphero_list = search_for_sphero()
            if(len(sphero_list) == 0):
                raise "No Sphero Found in Vicinity"
            addr = sphero_list[0]

        self._addr = addr
        self._connected = False
        self._seq_counter = 0
        self._stream_rate = 10
        #load the mask list
        with open(os.path.join(os.path.dirname(__file__),'data','mask_list.yaml'),'r') as mask_file:
            self._mask_list = yaml.load(mask_file)
        self._curr_data_mask = bytes.fromhex("0000 0000")
        self._curr_data_mask2 = bytes.fromhex("0000 0000")
    
    def get_power_state(self):
        """
        Get the power state of Sphero
        ----
        return - levels of battery (charging, critical, low, OK) and other information
        """
        seq_num = self._send_command("ff","00","20",[])
        response = self._notifier.wait_for_resp(seq_num)
        #parse the response packet and make sure it's correct
        if response and response[4]==9:
            return response[6]
        else:
            return None

    def get_collision(self):
        """
        Get the power state of Sphero
        ----
        return - levels of battery (charging, critical, low, OK) and other information
        """
        seq_num = self._send_command("ff","02","12",[binascii.a2b_hex("20"),binascii.a2b_hex("20"),binascii.a2b_hex("10"),binascii.a2b_hex("10"),binascii.a2b_hex("01")])
        response = self._notifier.wait_for_resp(seq_num)
        #parse the response packet and make sure it's correct
        if response:
            return response[5]
        else:
            return None
        
    def start_speed_callback(self,rate,callback):
        """
        Set a accelerator callback that streams the data to the callback

        callback - (function) function that we will pass the information when there is a callback
        """
        name = "Speed"
        #first we register the callback with the notifier
        self._notifier.register_async_callback(name,callback)
        #start data stream
        #handle mask
        self._handle_mask(name)
        #send the mask as data
        self._stream_rate = rate
        self._send_data_command(rate,(0).to_bytes(4,'big'),self._curr_data_mask)

    def start_collision_callback(self,callback):
        """
        Set a accelerator callback that streams the data to the callback

        callback - (function) function that we will pass the information when there is a callback
        """
        name = "Collision"
        #first we register the callback with the notifier
        self._notifier.register_async_callback(name,callback)
        #start data stream
        #send the mask as data
        data = [binascii.a2b_hex("01"),binascii.a2b_hex("40"), binascii.a2b_hex("40") ,binascii.a2b_hex("40"),binascii.a2b_hex("40"),binascii.a2b_hex("40")]
        data_list = self._format_data_array(data)
        sop1 = binascii.a2b_hex("ff")
        sop2 = binascii.a2b_hex("ff")
        did = binascii.a2b_hex("02")
        cid = binascii.a2b_hex("12")
        seq_val = self._get_sequence()
        seq = seq_val.to_bytes(1,"big")
        dlen = binascii.a2b_hex("07")
        packet = [sop1,sop2,did,cid,seq,dlen] + data_list
        packet += [cal_packet_checksum(packet[2:]).to_bytes(1,'big')] #calculate the checksum
        #write the command to Sphero
        #print("cmd:{} packet:{}".format(cid, b"".join(packet)))
        self._cmd_characteristics[CommandsCharacteristic].write(b"".join(packet))
        return seq_val 

    def start_odometer_callback(self,rate,callback):
        """
        Set a odometer callback that streams the data to the callback

        callback - (function) function that we will pass the information when there is a callback
        """
        name = "Odom"
        #first we register the callback with the notifier
        self._notifier.register_async_callback(name,callback)
        #start data stream
        #handle mask
        self._handle_mask(name)
        #send the mask as data
        self._stream_rate = rate
        self._send_data_command(rate,(0).to_bytes(4,'big'),self._curr_data_mask)

    def start_all_callback(self,rate,callback):
        """
        Set a odometer callback that streams the data to the callback

        callback - (function) function that we will pass the information when there is a callback
        """
        name = "All"
        #first we register the callback with the notifier
        self._notifier.register_async_callback(name,callback)
        #start data stream
        #handle mask
        self._handle_mask_v2(name)
        #send the mask as data
        self._stream_rate = rate
        self._send_data_command_v2(rate,self._curr_data_mask,self._curr_data_mask2)


#FONCTIONS A MODIFIER POUR ETRE GENERALES (COMPATIBLES AVEC MASK ET MASK2)
        
    def _handle_mask_v2(self,group_name, remove=False):

        if(remove):
            optr = XOR_mask
        else:
            optr = OR_mask
        for i,group in enumerate(self._mask_list):
            if(group["name"] == group_name):
                for i, value in enumerate(group["values"]):
                    for key in value:
                        if key=="mask":
                            self._curr_data_mask = optr(self._curr_data_mask, bytes.fromhex(value["mask"]))
                        elif key=="mask2" :
                            self._curr_data_mask2 = optr(self._curr_data_mask2, bytes.fromhex(value["mask2"]))
                            


    def _send_data_command_v2(self,rate,mask1,mask2,sample=1):
        N = ((int)(400/rate)).to_bytes(2,byteorder='big')
        #N = (40).to_bytes(2,byteorder='big')
        M = (sample).to_bytes(2,byteorder='big')
        PCNT = (0).to_bytes(1,'big')
        #MASK2 = (mask2).to_bytes(4,'big')
        data = [N,M, mask1 ,PCNT,mask2]
        self.command("11",data)


    def _stop_data_stream(self, group_name):
        #handle mask
        self._handle_mask(group_name,remove=True)
        self._send_data_command(self._stream_rate,self._curr_data_mask,(0).to_bytes(4,'big'))
