import pygame
from math import *
import time
from threading import Thread
from pygame.locals import *
import pygame.gfxdraw
import random

from tkinter import *
from tkinter.colorchooser import *

from sphero_sprk import Sphero, Sphero_Improved

# Define some colors
BLACK    = (   0,   0,   0)
WHITE    = ( 255, 255, 255)

#Sphero adress
sphero = Sphero_Improved("C1:63:90:78:89:47") 
sphero.connect()


# Left Joystick Axis :
# format(0,joystick.get_axis(0)) : x : Left = -1 ; Right = 1
# format(1,joystick.get_axis(1)) : y : Forward = -1 ; Backward = 1
# Value at center is variable but is always between -0.100 and + 0.100


#Some data we need
speed=[0,0] #max speed = 3000 mm/s = 3 m/s
odom=[0,0]
gyro=[0,0,0]
collision=[0]


#CALLBACKS

def f(x):
    speed[0]=x['xSpeed'] # en mm/s
    speed[1]=x['ySpeed']
    odom[0]=x['xOdom']
    odom[1]=x['yOdom']
    gyro[0]=x['pitch']
    gyro[1]=x['roll']
    gyro[2]=x['yaw']
    

def g(y):
    collision[0]=y
    

#Thread for callbacks
    
class Speed(Thread):

    def __init__(self):
        Thread.__init__(self)


    def run(self):
        """Code to be executed"""
        sphero.start_collision_callback(g)
        sphero.start_all_callback(10,f)
        

#Threads launching
thread_1 = Speed()
thread_1.start()


#Some useful functions

def module(x,y):
    mod = sqrt(x*x+y*y)
    if (mod<0.001):
        mod=0.001
    return mod

def find_angle(x,y):
    mod=module(x,y)
    angle=acos(x/mod);
    if (asin(y/mod)<0):
        angle=-angle
    return angle

def convert_rad_to_deg(angle):
    return round(((angle*180)/pi)%360)

def angle_for_sphero_from_angle(angle):
    return round((angle-270)%360)

def angle_for_sphero(x,y): # return angle following this rule : 0 = Forward ; 90 = Left ; 180 = Backward ; 270 = Right
    return angle_for_sphero_from_angle(convert_rad_to_deg(find_angle(x,y)))

def speed_for_sphero(x,y,sensitivity): # return speed between 0 and 255
    z=max(abs(x),abs(y))
    if (z<0.07): # Limit under which the joystick can be calibrated not properly so it's like the axis is at the centre
        z=0
    else:
        z=round((z-0.07)*274.19*sensitivity)
    return z


# A class to help display data about joysticks in pygame
# It has nothing to do with the joysticks, just outputting the information.
class TextPrint:

    def __init__(self):
        self.reset()
        self.font = pygame.font.Font(None, 20)

    def print(self, screen, textString):
        textBitmap = self.font.render(textString, True, BLACK)
        screen.blit(textBitmap, [self.x, self.y])
        self.y += self.line_height

    def reset(self):
        self.x = 10
        self.y = 10
        self.line_height = 15

    def indent(self):
        self.x += 10

    def unindent(self):
        self.x -= 10



# Class with functions to control Sphero
class SpheroGame :

    def move(self,x1,y1):
        self.speed=speed_for_sphero(x1,y1,self.sensitivity)
        angle_temp = angle_for_sphero(x1,y1)
        sphero.roll(self.speed,(self.direction+angle_temp)%360)

    def set_direction(self,x2,y2):
        self.direction=angle_for_sphero(x2,y2)
        sphero.roll(100,self.direction)
        sphero.roll(0,self.direction)
        sphero.set_heading(self.direction)

    def change_sensitivity(self,difference): #difference = -0.1 or +0.1 (11 levels of sensitivity from 0.0 to 1.0)
        self.sensitivity = max(0,min(1,self.sensitivity + difference))

    def __init__(self):
        # Start Sphero Control
        
        input_random = input("Mode random (y/n) ? ")
        mode_random=False
        if (input_random=="y"):
            mode_random=True

        self.direction = 0
        self.speed = 0
        self.sensitivity = 0.2
        self.boom=collision[0]

        ledColor=sphero.get_rgb_led()

        pygame.init()

        # Set the width and height of the screen [width,height]
        size = [800, 800]
        screen = pygame.display.set_mode(size)

        pygame.display.set_caption("Sphero Controller")

        #Loop until the user clicks the close button.
        done = False

        # Used to manage how fast the screen updates
        clock = pygame.time.Clock()

        # Initialize the joysticks
        pygame.joystick.init()

        # Get ready to print
        textPrint = TextPrint()

        if (done):
            textPrint.print(screen, "Please connect a Joystick")
            print("Please connect a Joystick")
            done=True
        else:
            joystick = pygame.joystick.Joystick(0)
            joystick.init()

            angle=0
            angle2=0

            image_background = pygame.image.load("images_sphero/background.jpg")
            image_ok = pygame.image.load("images_sphero/battery_ok.png")
            image_low = pygame.image.load("images_sphero/battery_low.png")
            image_critical = pygame.image.load("images_sphero/battery_critical.png")
            image_charging = pygame.image.load("images_sphero/battery_charging.png")
            image_dashboard = pygame.image.load("images_sphero/dashboard.png")
            image_compteur = pygame.image.load("images_sphero/compteur.png")
            image_aiguille = pygame.image.load("images_sphero/aiguille.png")
            image_assiette = pygame.image.load("images_sphero/assiette.png")
            image_horizon = pygame.image.load("images_sphero/horizon.png")
            image_compteur = pygame.transform.scale(image_compteur, (250, 250))
            image_aiguille = pygame.transform.scale(image_aiguille, (250, 250))
            image_assiette = pygame.transform.scale(image_assiette, (250, 250))
            image_horizon = pygame.transform.scale(image_horizon, (250, 250))
            image_ok = pygame.transform.scale(image_ok, (65, 33))
            image_low = pygame.transform.scale(image_low, (65, 33))
            image_critical = pygame.transform.scale(image_critical, (65, 33))
            image_charging = pygame.transform.scale(image_charging, (65, 33))
            color_picker = pygame.image.load("images_sphero/button.png")


            t=[[400,200]]
            tbis=[[400,200]]
            odomxshift=t[0][0]-odom[0]
            odomyshift=t[0][1]-odom[1]
            xshift=0
            yshift=0
            
            compteur=0

            xmin=200
            xmax=600
            ymin=100
            ymax=300

            scale=1

            screen.blit(image_background,(0,0))
            pygame.draw.rect(screen,pygame.Color(255, 255, 255),(200,100,400,200),0)
            pygame.draw.rect(screen,pygame.Color(0, 0, 0),(200,100,400,200),3)

            direct=0
            listcoll=[]
            listcollbis=[]
            
            sphero.set_heading(0)
            print(odom[0])
            print(odom[1])
            
            # -------- Main Program Loop -----------
            while done==False:
                compteur+=1

                if (max(abs(joystick.get_axis(2)),abs(joystick.get_axis(3)))>0.9):
                    self.set_direction(joystick.get_axis(2),joystick.get_axis(3))

                if (mode_random):
                    if self.boom!=collision[0]:
                        listcoll.append([t[-1][0],t[-1][1]])
                        xnew=(t[-1][0]//scale)+xshift
                        ynew=(t[-1][1]//scale)+yshift
                        listcollbis.append([xnew,ynew])
                        sphero.roll(0,self.direction)
                        self.direction=random.randint(min((self.direction+90)%360,(self.direction+270)%360),max((self.direction+90)%360,(self.direction+270)%360))
                        self.boom=collision[0]
                    elif speed[0]<8 and speed[1]<8:
                        direct+=1
                        if direct==1 :
                            direct+=1
                            self.direction=random.randint(min((self.direction+90)%360,(self.direction+270)%360),max((self.direction+90)%360,(self.direction+270)%360))
                        elif direct==10 :
                            direct=0
                            

                    sphero.roll(50,self.direction)
                else: 
                    self.move(joystick.get_axis(0),joystick.get_axis(1))


                if (joystick.get_button(4)):
                    self.change_sensitivity(-0.1)
                elif (joystick.get_button(5)):
                    self.change_sensitivity(0.1)
                else:
                    k=0
                    #TO-DO (Actions when pressing other buttons)
                
                for event in pygame.event.get(): # User did something
                    if event.type == pygame.QUIT: # If user clicked close
                        done=True # Flag that we are done so we exit this loop
            
                    # Possible joystick actions: JOYAXISMOTION JOYBALLMOTION JOYBUTTONDOWN JOYBUTTONUP JOYHATMOTION
                    if event.type == pygame.JOYBUTTONDOWN:
                        #print("Joystick button pressed.")
                        i=0
                    if event.type == pygame.JOYBUTTONUP:
                        #print("Joystick button released.")
                        i=0
                    if event.type == pygame.MOUSEBUTTONUP:
                        x,y=event.pos
                        x-=445
                        y-=420
                        center=[(440+440+75)/2,(410+410+75)/2]
                        print(color_picker.get_rect().collidepoint(x,y))
                        if color_picker.get_rect().collidepoint(x,y):
                            #ColorPicker
                            window=Tk()
                            result=askcolor(ledColor,parent=window)
                            window.destroy()
                            if result[0] is not None:
                                ledColor=result[0]
                                sphero.set_rgb_led(floor(ledColor[0]),floor(ledColor[1]),floor(ledColor[2]))
                            
                        i=0
                
                
                # DRAWING STEP
                # First, clear the screen to white. Don't put other drawing commands
                # above this, or they will be erased with this command.

                rect_aiguille = image_aiguille.get_rect(center=(588,607))
                rect_horizon = image_horizon.get_rect(center=(207,607))
                aiguille = pygame.transform.rotate(image_aiguille, angle)
                horizon = pygame.transform.rotate(image_horizon, angle2)
                rect_aiguille = aiguille.get_rect(center=rect_aiguille.center)
                rect_horizon = horizon.get_rect(center=rect_horizon.center)
                screen.blit(image_dashboard,(0,296))
                screen.blit(image_compteur,(463,482))
                screen.blit(color_picker,(445,420))
                screen.blit(horizon,rect_horizon)
                screen.blit(image_assiette,(82,482))
                screen.blit(aiguille,rect_aiguille)
                
                t.append([odom[0]+odomxshift,odom[1]+odomyshift])

                xnew=(t[-1][0]//scale)+xshift
                ynew=(t[-1][1]//scale)+yshift

                tbis.append([xnew,ynew])

                if ((tbis[-1][0]<xmin+1 or tbis[-1][0]>xmax-1 or tbis[-1][1]<ymin+1 or tbis[-1][1]>ymax-1)):
                    print(odom[0])
                    print(odom[1])
                    pygame.draw.rect(screen,pygame.Color(255, 255, 255),(200,100,400,200),0)
                    pygame.draw.rect(screen,pygame.Color(0, 0, 0),(200,100,400,200),3)
                    scale+=1
                    tbis=[[t[0][0],t[0][1]]]

                    xshift=t[0][0]-t[0][0]//scale
                    yshift=t[0][1]-t[0][1]//scale

                    for i in range (1,len(t)):
                        xnew=(t[i][0]//scale)+xshift
                        ynew=(t[i][1]//scale)+yshift
                        
                        tbis.append([xnew, ynew])
                        pygame.draw.aaline(screen, (255,0,0), [tbis[-2][0], tbis[-2][1]], [tbis[-1][0], tbis[-1][1]], True)

                    listcollbis=[]
                    for x in listcoll:
                        xnew=(x[0]//scale)+xshift
                        ynew=(x[1]//scale)+yshift
                        
                        listcollbis.append([xnew, ynew])

                pygame.draw.aaline(screen, (255,0,0), [tbis[-2][0], tbis[-2][1]], [tbis[-1][0], tbis[-1][1]], True)

                for x in listcollbis:
                    pygame.gfxdraw.filled_circle(screen,x[0],x[1],3,(0,0,255))

                #Battery view
                battery=sphero.get_power_state()
                if (battery==4):
                    screen.blit(image_critical,(358,465))
                elif (battery==3):
                    screen.blit(image_low,(358,465))
                elif (battery==2):
                    screen.blit(image_ok,(358,465))
                else:
                    screen.blit(image_charging,(358,465))

                # ALL CODE TO DRAW SHOULD GO ABOVE THIS COMMENT

                # Go ahead and update the screen with what we've drawn.
                pygame.display.flip()
                
                #angle goes from 83 to 0 and then from 359 to 173 = 270 step
                angle=0
                
                angle=module(speed[0],speed[1])*270/3000
                if (angle<=83):
                    angle=83-angle
                    
                else :
                    angle=443-angle

                #angle2 is for the roll angle of Sphero
                angle2=gyro[1]
                
                # Limit to 20 frames per second
                clock.tick(24)

            # Close the window and quit.
            # If you forget this line, the program will 'hang'
            # on exit if running from IDLE.
            pygame.quit ()

sg = SpheroGame()
